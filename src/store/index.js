/**
 *  @desc 
 *  @param
 *  @retrurn 
 */

import Vue from 'vue'
import Vuex from 'vuex'

// Installing Vuex via Vue.use()
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    routeName: { bachelorPath: 'Bachelor-show', internshipPath: 'Internship-show'}
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
