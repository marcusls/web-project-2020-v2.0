/**
 *  @desc creating a new instance of Axios as API client to load remote data
 *  @retrurn request methods
 */

// Imports the axios library
import axios from "axios";

// creating a new client instance of Axios at localhost:3000 and excepting json files
const apiClient = axios.create({
  baseURL: "http://localhost:3000",
  withCredentials: false, // default
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json"
  }
});

// Provinding request methods with  baseURL and './path'
export default {
  getItems(path) {
    return apiClient.get(path);
  },
  getItem(path, id) {
    return apiClient.get(path + id);
  }
};
