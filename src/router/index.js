/**
 *  @desc Using Vue Router to map components to path routes and where to render them
 *  @param Vue router from vue library and view components for src/view
 *  @retrurn Vue router object containing mapping and routes
 */

 // Importing components
import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue';
import LoginShow from '../views/LoginShow.vue';
import StudBachList from '../views/StudBachList.vue'
import BachelorShow from '../views/BachelorShow.vue';
import StudIntList from '../views/StudIntList.vue';
import InternshipShow from '../views/InternshipShow.vue';
import Inbox from '../views/Inbox.vue';
import SignupShow from '../views/SignupShow.vue';
import RegisterForm from '../views/RegisterForm.vue';
import bachelorContract from '../components/bachelorContract.vue';
import internshipContract from '../components/internshipContract.vue';

// Installing the router via Vue.use()
Vue.use(VueRouter)

// Defining routes. Each route matches to a components.
const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/login',
    name: 'Login',
    component: LoginShow
  },
  {
    path: '/student-bachelor-list',
    name: 'StudBachList',
    component: StudBachList
  },
  {
    path: '/student-bachelor-list/:id',
    name: 'Bachelor-show',
    component: BachelorShow,
    props: true
  },
  {
    path: '/student-internship-list',
    name: 'StudIntList',
    component: StudIntList
  },
  {
    path: '/student-internship-list/:id',
    name: 'Internship-show',
    component: InternshipShow,
    props: true
  },
  {
    path: '/signup',
    name: 'Signup',
    component: SignupShow
  },
  {
    path:'/inbox',
    name: 'Inbox',
    component: Inbox
  },
  {
    path: '/register-form',
    name: 'RegisterForm',
    component: RegisterForm
  },
  {
    path: '/bachelor-contract',
    name: 'bachelorContract',
    component: bachelorContract
  },
  {
    path: '/internship-contract',
    name: 'internshipContract',
    component: internshipContract
  },
]

//  Creating the router instance and passing the 'routes'
const router = new VueRouter ({
  mode: "history",
  routes
})

export default router
