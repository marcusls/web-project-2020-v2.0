# web-project-2


--Starte/kjøre prosjektet--
Comand lines

npm, vue og json-server må være installert: npm install npm@latest -g || npm install -g @vue/cli || npm install -g json-server

* npm run serve
    - Starter Vue prosjektet på localhost
* vue ui
    - Legg til/last ned dependensies fra vue ui: axios og lodash
* json-server --watch db.json
    - Starter en json dev server på localhost:3000
        - "db.json" er navet på filen som inneholder "databasen". Skal ligge i root



*****README****

IMT2671 - Webproject 2

Members:
Marcus Leikfoss Swensen - Team Leader
Lars Erik Lunde - Main Developer
Wisarut Mortensen - Designer/Developer
Zakariya Afrah - Main Design


*****Guidelines****

* Write your code in english.
    - Name variables and comment in english.

* Comment on everything you code and change.

* Commit often and always pull before you push.

* Do not work in master brach

* Do not commit and push to master branch unless it is finished code



## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
